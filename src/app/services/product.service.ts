import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { share, tap } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-type': 'application/json',
    Accept: '*/*',
    responseType: 'json',
  }),
};

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  private rootUrl: string;
  private actionUrl: string = '';

  constructor(private http: HttpClient) {
    this.rootUrl = `${environment.baseUrl.server}`;
  }

  getAllProducts(postcode: string) {
    this.actionUrl = `product/getlistproduct/${postcode}`;
    return this.http
      .get(`${this.rootUrl}${this.actionUrl}`, httpOptions)
      .pipe(tap(console.log), share());
  }
}
