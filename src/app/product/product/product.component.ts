import { AfterViewInit, Component, OnInit } from '@angular/core';
import { ProductService } from '../../services/product.service';
import { MapMarker } from '@angular/google-maps';
import {
  FormControl,
  FormGroup,
  FormBuilder,
  Validators,
} from '@angular/forms';
import { FindValueSubscriber } from 'rxjs/internal/operators/find';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'],
})
export class ProductComponent implements OnInit, AfterViewInit {
  allProducts: any = [];
  lat: string = '';
  lng: string = '';
  productForm!: FormGroup;
  isProductList: boolean = false;
  zoom = 20;
  center!: google.maps.LatLngLiteral;
  options: google.maps.MapOptions = {
    mapTypeId: 'hybrid',
    zoomControl: false,
    scrollwheel: false,
    disableDoubleClickZoom: true,
    maxZoom: 35,
    minZoom: 8,
  };
  markers: any = [];
  marker: any;
  postcode: string = '';
  infoContent = '';
  map!: google.maps.Map;
  showMap: boolean = false;

  constructor(
    private productService: ProductService,
    private formBuilder: FormBuilder
  ) {
    this.productForm = this.formBuilder.group({
      postcode: new FormControl('', [Validators.required]),
    });
  }

  ngOnInit(): void {}

  ngAfterViewInit() {
    this.marker = { position: { lat: +this.lat, lng: +this.lng } };
    this.markers = [this.marker];
  }

  get f() {
    return this.productForm.controls;
  }

  onSubmitProduct(event: Event) {
    debugger;
    event.preventDefault();

    //Get all products
    this.postcode = this.productForm.controls['postcode'].value;
    this.productService.getAllProducts(this.postcode).subscribe((response) => {
      debugger;
      if (response.productList) {
        this.allProducts = response.productList;
        this.lat = response.coordinate.lat;
        this.lng = response.coordinate.lng;
        this.isProductList = true;
      } else {
        console.log(response);
        this.isProductList = false;
      }
    });
  }

  getLocation() {
    this.showMap = true;
    navigator.geolocation.getCurrentPosition(() => {
      this.center = {
        lat: +this.lat,
        lng: +this.lng,
      };
    });

    this.marker = { position: { lat: +this.lat, lng: +this.lng } };
    this.markers = [this.marker];
  }

  zoomIn() {
    if (this.options.maxZoom) {
      if (this.zoom < this.options.maxZoom) this.zoom++;
    }
  }

  zoomOut() {
    if (this.options.minZoom) {
      if (this.zoom > this.options.minZoom) this.zoom--;
    }
  }
}
